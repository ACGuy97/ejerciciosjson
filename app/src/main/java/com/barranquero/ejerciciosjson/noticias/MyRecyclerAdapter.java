package com.barranquero.ejerciciosjson.noticias;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.barranquero.ejerciciosjson.R;
import com.barranquero.ejerciciosjson.modelos.NewsFeed;
import com.barranquero.ejerciciosjson.utilidades.NetworkController;

import java.util.List;

/**
 * * g o a t s e x * g o a t s e x * g o a t s e x *
 * g                                               g
 * o /     \             \            /    \       o
 * a|       |             \          |      |      a
 * t|       `.             |         |       :     t
 * s`        |             |        \|       |     s
 * e \       | /       /  \\\   --__ \\       :    e
 * x  \      \/   _--~~          ~--__| \     |    x
 * *   \      \_-~                    ~-_\    |    *
 * g    \_     \        _.--------.______\|   |    g
 * o      \     \______// _ ___ _ (_(__>  \   |    o
 * a       \   .  C ___)  ______ (_(____>  |  /    a
 * t       /\ |   C ____)/      \ (_____>  |_/     t
 * s      / /\|   C_____)       |  (___>   /  \    s
 * e     |   (   _C_____)\______/  // _/ /     \   e
 * x     |    \  |__   \\_________// (__/       |  x
 * *    | \    \____)   `----   --'             |  *
 * g    |  \_          ___\       /_          _/ | g
 * o   |              /    |     |  \            | o
 * a   |             |    /       \  \           | a
 * t   |          / /    |         |  \           |t
 * s   |         / /      \__/\___/    |          |s
 * e  |           /        |    |       |         |e
 * x  |          |         |    |       |         |x
 * * g o a t s e x * g o a t s e x * g o a t s e x *
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {
    private List<NewsFeed> feedsList;
    private Context context;
    private LayoutInflater inflater;

    public MyRecyclerAdapter(Context context, List<NewsFeed> feedsList) {
        this.context = context;
        this.feedsList = feedsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.singleitem, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewsFeed feed = feedsList.get(position);
//Pass the values of feeds object to Views
        holder.title.setText(feed.getTitle());
        holder.description.setText(feed.getDescription());
        holder.imageview.setImageUrl(feed.getImage(),
                NetworkController.getInstance(context).getImageLoader());
    }
    @Override
    public int getItemCount() {
        return feedsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView title, description;
        private NetworkImageView imageview;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_view);
            description = (TextView) itemView.findViewById(R.id.description_view);
// Volley's NetworkImageView which will load Image from URL
            imageview = (NetworkImageView) itemView.findViewById(R.id.thumbnail_view);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                Toast.makeText(context, "Date: " + feedsList.get(position).getPubDate(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public boolean onLongClick(View v) {
            int position = getAdapterPosition();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(feedsList.get(position).getLink()));
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                Toast.makeText(context, "No hay navegador", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    }
}
