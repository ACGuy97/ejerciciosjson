package com.barranquero.ejerciciosjson.contactos;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.barranquero.ejerciciosjson.R;
import com.barranquero.ejerciciosjson.modelos.Contazto;
import com.barranquero.ejerciciosjson.utilidades.Analisis;
import com.barranquero.ejerciciosjson.utilidades.RestClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ListaContactos_Activity extends AppCompatActivity {
    public static final String URL_CONTAZTOS = "http://bitbits.hopto.org/ACDAT/json/contaztos.json";
    Button btnObtener;
    ListView lsContaztos;
    ArrayList<Contazto> misContaztos;
    ArrayAdapter<Contazto> miAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_contactos);
        setTitle("Contaztos");

        btnObtener = (Button) findViewById(R.id.btnObtener);
        btnObtener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descarga(URL_CONTAZTOS);
            }
        });

        lsContaztos = (ListView) findViewById(R.id.lsContazto);
        lsContaztos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListaContactos_Activity.this, "Móvil: " + misContaztos.get(position).getTelefono().getMovil(), Toast. LENGTH_SHORT).show();
            }
        });
    }

    private void descarga(String tuURl) {

            final ProgressDialog progreso = new ProgressDialog(this);
            RestClient.get(tuURl, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progreso.setProgressStyle(ProgressDialog. STYLE_SPINNER );
                    progreso.setMessage("Conectando . . .");
                    progreso.setCancelable(true);
                    progreso.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progreso.dismiss();
                    try {
                        misContaztos = Analisis.analizarContaztos(response);
                        mostrar();
                    } catch (JSONException e) {
                        Toast.makeText(ListaContactos_Activity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progreso.dismiss();
                    Toast.makeText(ListaContactos_Activity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            } );

    }

    private void mostrar() {
        if (misContaztos != null)
            if (miAdapter == null) {
                miAdapter= new ArrayAdapter<Contazto>(this, android.R.layout. simple_list_item_1 , misContaztos);
                lsContaztos.setAdapter(miAdapter);
            } else {
                miAdapter.clear();
                miAdapter.addAll(misContaztos);
            }
        else
            Toast.makeText(getApplicationContext(), "Error al crear la lista", Toast. LENGTH_SHORT ).show();
    }
}
