package com.barranquero.ejerciciosjson.primitiva;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.barranquero.ejerciciosjson.R;
import com.barranquero.ejerciciosjson.utilidades.Analisis;
import com.barranquero.ejerciciosjson.utilidades.Memoria;
import com.barranquero.ejerciciosjson.utilidades.Resultado;

import org.json.JSONException;

public class Primitiva_Activity extends AppCompatActivity {
    TextView informacion;
    Memoria memoria;
    Resultado resultado;
    public static final String FICHERO_PRIMITIVA = "primitiva.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primitiva);
        setTitle("Primitiva");

        informacion = (TextView) findViewById(R.id.txvPrimitiva);
        memoria = new Memoria(this);
        resultado = memoria.leerAsset( FICHERO_PRIMITIVA );
        if (resultado.isCodigo()) {
            try {
                informacion.setText(Analisis.analizarPrimitiva(resultado.getContenido()));
            } catch (JSONException e) {
                informacion.setText(e.getLocalizedMessage());
            }
        }
    }
}
