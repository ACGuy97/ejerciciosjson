package com.barranquero.ejerciciosjson.primitiva;

import android.net.http.RequestQueue;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.barranquero.ejerciciosjson.R;
import com.barranquero.ejerciciosjson.utilidades.Analisis;
import com.barranquero.ejerciciosjson.utilidades.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class PrimitivaRed_Activity extends AppCompatActivity {
    public static final String TAG = "MyTag";
    public static final String WEB = "http://bitbits.hopto.org/ACDAT/json/primitiva.json";
    TextView mTextView;
    com.android.volley.RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primitiva_red);
        setTitle("Primitiva en Red");

        mTextView = (TextView) findViewById(R.id.txvPrimRed);
        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
    }

    public void onClickPrimitiva(View view) {
        descarga();
    }

    private void descarga() {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, WEB, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mTextView.setText(Analisis.analizarPrimitiva(response));
                            Toast.makeText(PrimitivaRed_Activity.this, "Descargado con éxito", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            mTextView.setText(e.getLocalizedMessage());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mTextView.setText(error.getLocalizedMessage());
                    }
                });

// Set the tag on the request.
        jsObjRequest.setTag(TAG);
// Set retry policy
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
// Add the request to the RequestQueue.
        mRequestQueue.add(jsObjRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }
}
