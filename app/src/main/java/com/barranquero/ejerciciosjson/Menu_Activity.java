package com.barranquero.ejerciciosjson;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.barranquero.ejerciciosjson.contactos.ListaContactosGSON_Activity;
import com.barranquero.ejerciciosjson.contactos.ListaContactos_Activity;
import com.barranquero.ejerciciosjson.crear.Creacion_Activity;
import com.barranquero.ejerciciosjson.noticias.Noticias_Activity;
import com.barranquero.ejerciciosjson.primitiva.PrimitivaRed_Activity;
import com.barranquero.ejerciciosjson.primitiva.Primitiva_Activity;

public class Menu_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setTitle("Mi menú");
    }

    public void onClickMenu(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn1:
                intent = new Intent(this, Primitiva_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn2:
                intent = new Intent(this, PrimitivaRed_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn3:
                intent = new Intent(this, ListaContactos_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn4:
                intent = new Intent(this, ListaContactosGSON_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn5:
                intent = new Intent(this, Creacion_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn6:
                intent = new Intent(this, Noticias_Activity.class);
                startActivity(intent);
                break;
        }
    }
}
