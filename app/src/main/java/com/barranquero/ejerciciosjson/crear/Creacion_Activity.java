package com.barranquero.ejerciciosjson.crear;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.barranquero.ejerciciosjson.R;
import com.barranquero.ejerciciosjson.modelos.Noticia;
import com.barranquero.ejerciciosjson.utilidades.Analisis;
import com.barranquero.ejerciciosjson.utilidades.RestClient;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Creacion_Activity extends AppCompatActivity {
    public static final String URL = "http://www.alejandrosuarez.es/feed/";
    public static final String RESULTADO_JSON = "resultado.json";
    public static final String RESULTADO_GSON = "resultado_gson.json";
    public static final String TEMPORAL = "alejandro.xml";
    ArrayList<Noticia> noticias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creacion);
        setTitle("Crear JSON");

        descarga(URL, TEMPORAL);
    }

    private void descarga(String web, String temporal) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        RestClient.get(web, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(Creacion_Activity.this, "\"Fallo: \" + statusCode + \"\\n\" + throwable.getMessage()", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                try {
                    //informacion.setText(Analisis.analizarRSS(file));
                    noticias = Analisis.analizarNoticias(file);
                    Analisis.escribirJSON(noticias, RESULTADO_JSON);
                    Analisis.escribirGSON(noticias, RESULTADO_GSON);
                    Toast.makeText(Creacion_Activity.this, "Hecho", Toast.LENGTH_SHORT).show();
                } catch (XmlPullParserException e) {
                    Log.e("XMP ", e.getMessage());
                } catch (IOException e) {
                    Log.e("IO ", e.getMessage());
                } catch (JSONException e) {
                    Log.e("JSON", e.getMessage());
                }
            }
        });
    }
}
