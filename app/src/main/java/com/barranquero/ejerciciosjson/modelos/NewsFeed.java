package com.barranquero.ejerciciosjson.modelos;

/**
 * * g o a t s e x * g o a t s e x * g o a t s e x *
 * g                                               g
 * o /     \             \            /    \       o
 * a|       |             \          |      |      a
 * t|       `.             |         |       :     t
 * s`        |             |        \|       |     s
 * e \       | /       /  \\\   --__ \\       :    e
 * x  \      \/   _--~~          ~--__| \     |    x
 * *   \      \_-~                    ~-_\    |    *
 * g    \_     \        _.--------.______\|   |    g
 * o      \     \______// _ ___ _ (_(__>  \   |    o
 * a       \   .  C ___)  ______ (_(____>  |  /    a
 * t       /\ |   C ____)/      \ (_____>  |_/     t
 * s      / /\|   C_____)       |  (___>   /  \    s
 * e     |   (   _C_____)\______/  // _/ /     \   e
 * x     |    \  |__   \\_________// (__/       |  x
 * *    | \    \____)   `----   --'             |  *
 * g    |  \_          ___\       /_          _/ | g
 * o   |              /    |     |  \            | o
 * a   |             |    /       \  \           | a
 * t   |          / /    |         |  \           |t
 * s   |         / /      \__/\___/    |          |s
 * e  |           /        |    |       |         |e
 * x  |          |         |    |       |         |x
 * * g o a t s e x * g o a t s e x * g o a t s e x *
 */

public class NewsFeed {
    private String title, link, description, image, pubDate;
    public NewsFeed(String title, String link, String description, String image, String pubDate) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.image = image;
        this.pubDate = pubDate;
    }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public String getLink() { return link; }
    public void setLink(String link) { this.link = link; }
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }
    public String getImage() { return image; }
    public void setImage(String image) { this.image = image; }
    public String getPubDate() { return pubDate; }
    public void setPubDate(String pubDate) { this.pubDate = pubDate; }
}
