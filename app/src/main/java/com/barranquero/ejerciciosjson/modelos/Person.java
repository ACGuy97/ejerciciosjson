package com.barranquero.ejerciciosjson.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by usuario on 20/12/16.
 */

public class Person {
    @SerializedName("contacts")
    public List<Contazt> contazts;
    public Person() {
    }
    public List<Contazt> getContaztos() {
        return contazts;
    }
    public void setContaztos(List<Contazt> contacts) {
        this.contazts = contacts;
    }
}
