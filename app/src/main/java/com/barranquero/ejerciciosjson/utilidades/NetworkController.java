package com.barranquero.ejerciciosjson.utilidades;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import okhttp3.OkHttpClient;

/**
 * * g o a t s e x * g o a t s e x * g o a t s e x *
 * g                                               g
 * o /     \             \            /    \       o
 * a|       |             \          |      |      a
 * t|       `.             |         |       :     t
 * s`        |             |        \|       |     s
 * e \       | /       /  \\\   --__ \\       :    e
 * x  \      \/   _--~~          ~--__| \     |    x
 * *   \      \_-~                    ~-_\    |    *
 * g    \_     \        _.--------.______\|   |    g
 * o      \     \______// _ ___ _ (_(__>  \   |    o
 * a       \   .  C ___)  ______ (_(____>  |  /    a
 * t       /\ |   C ____)/      \ (_____>  |_/     t
 * s      / /\|   C_____)       |  (___>   /  \    s
 * e     |   (   _C_____)\______/  // _/ /     \   e
 * x     |    \  |__   \\_________// (__/       |  x
 * *    | \    \____)   `----   --'             |  *
 * g    |  \_          ___\       /_          _/ | g
 * o   |              /    |     |  \            | o
 * a   |             |    /       \  \           | a
 * t   |          / /    |         |  \           |t
 * s   |         / /      \__/\___/    |          |s
 * e  |           /        |    |       |         |e
 * x  |          |         |    |       |         |x
 * * g o a t s e x * g o a t s e x * g o a t s e x *
 */

public class NetworkController {

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mCtx;
    private static NetworkController mInstance;

    private NetworkController(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
// This will Load Images from Network in Separate Thread
        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    //Create ImageCache of max size 10MB
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(10);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                }


        );
    }

    public static synchronized NetworkController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkController(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
//mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext(), new OkHttp3Stack(new OkHttpClient()));
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }
}