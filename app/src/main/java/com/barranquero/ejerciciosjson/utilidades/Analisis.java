package com.barranquero.ejerciciosjson.utilidades;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

import com.barranquero.ejerciciosjson.modelos.Contazto;
import com.barranquero.ejerciciosjson.modelos.Noticia;
import com.barranquero.ejerciciosjson.modelos.Telefono;
import com.barranquero.ejerciciosjson.modelos.Titular;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by usuario on 13/12/16.
 */

public class Analisis {

    public static String analizarPrimitiva(String texto) throws JSONException {
        JSONObject jsonObjeto, item;
        JSONArray jsonContenido;
        String tipo;
        String datos;
        StringBuilder cadena = new StringBuilder();
        jsonObjeto = new JSONObject(texto);
        tipo = jsonObjeto.getString("info");
        /*datos = jsonObjeto.getString("sorteo");   una manera
        jsonContenido = new JSONArray(datos);*/
        jsonContenido = new JSONArray(jsonObjeto.getString("sorteo"));  //otra manera
        //jsonContenido = new JSONArray(jsonObjeto.getJSONArray("sorteo"));   otra manera; API > 19
        cadena.append("Sorteos de la Primitiva:" + "\n");
        for (int i = 0; i < jsonContenido.length(); i++) {
            item = jsonContenido.getJSONObject(i);
            cadena.append(tipo + ". " + item.getString("fecha") + "\n");
            cadena.append(item.getInt("numero1") + ", " + item.getInt("numero2") + ", " + item.getInt("numero3") + ", " + item.getInt("numero4") + ", " + item.getInt("numero5") + ", " + item.getInt("numero6") + "\n");
        }
        return cadena.toString();
    }

    public static String analizarPrimitiva(JSONObject objeto) throws JSONException {
        JSONObject jsonObjeto, item;
        JSONArray jsonContenido;
        String tipo;
        String datos;
        StringBuilder cadena = new StringBuilder();
        tipo = objeto.getString("info");
        /*datos = jsonObjeto.getString("sorteo");   una manera
        jsonContenido = new JSONArray(datos);*/
        jsonContenido = new JSONArray(objeto.getString("sorteo"));  //otra manera
        //jsonContenido = new JSONArray(jsonObjeto.getJSONArray("sorteo"));   otra manera; API > 19
        cadena.append("Sorteos de la Primitiva:" + "\n");
        for (int i = 0; i < jsonContenido.length(); i++) {
            item = jsonContenido.getJSONObject(i);
            cadena.append(tipo + ". " + item.getString("fecha") + "\n");
            cadena.append(item.getInt("numero1") + ", " + item.getInt("numero2") + ", " + item.getInt("numero3") + ", " + item.getInt("numero4") + ", " + item.getInt("numero5") + ", " + item.getInt("numero6") + "\n");
        }
        return cadena.toString();
    }

    public static ArrayList<Contazto> analizarContaztos(JSONObject respuesta) throws JSONException {
        JSONArray jAcontaztos;
        JSONObject jOcontazto, jOtelefono;
        Contazto contazto;
        Telefono telefono;
        ArrayList<Contazto> personas = null;
// añadir contactos de texto (en JSON) a personas
        jAcontaztos = respuesta.getJSONArray("contactos");
        personas = new ArrayList<Contazto>();

        for (int i = 0; i < jAcontaztos.length(); i++) {
            jOcontazto = jAcontaztos.getJSONObject(i);
            contazto = new Contazto();
            contazto.setNombre(jOcontazto.getString("nombre"));
            contazto.setDireccion(jOcontazto.getString("direccion"));
            contazto.setEmail(jOcontazto.getString("email"));
            jOtelefono = jOcontazto.getJSONObject("telefono");

            telefono = new Telefono();
            telefono.setCasa(jOtelefono.getString("casa"));
            telefono.setTrabajo(jOtelefono.getString("trabajo"));
            telefono.setMovil(jOtelefono.getString("movil"));
            contazto.setTelefono(telefono);

            personas.add(contazto);
        }

        return personas;
    }

    public static void escribirJSON(ArrayList<Noticia> noticias, String fichero) throws IOException, JSONException {
        OutputStreamWriter out;
        File miFichero;
        JSONObject objeto, miRss, item;
        JSONArray lista;
        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        out = new FileWriter(miFichero);
//crear objeto JSON
        objeto = new JSONObject();
        objeto.put("web", "http://www.alejandrosuarez.es/");
        objeto.put("link", "http://www.alejandrosuarez.es/feed/");
        lista = new JSONArray();

        for (int i = 0; i < noticias.size(); i++) {
            item = new JSONObject();
            item.put("titular", noticias.get(i).getTitle());
            item.put("fecha", noticias.get(i).getPubDate());
            item.put("descripcion", noticias.get(i).getDescription());
            item.put("enlace", noticias.get(i).getLink());
            lista.put(item);
        }

        objeto.put("titulares", lista);

        miRss = new JSONObject();
        miRss.put("rss", objeto);

        out.write(miRss.toString(4)); //tabulación de 4 caracteres
        out.flush();
        out.close();
        Log.i("info", objeto.toString());
    }

    public static ArrayList<Noticia> analizarNoticias(File file) throws XmlPullParserException, IOException {
        int eventType;
        ArrayList<Noticia> noticias = null;
        Noticia actual = null;
        boolean dentroItem = false;
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    noticias = new ArrayList<>();
                    break;
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        dentroItem = true;
                        actual = new Noticia();
                    } else if (xpp.getName().equalsIgnoreCase("title") && dentroItem) {
                        actual.setTitle(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("link") && dentroItem) {
                        actual.setLink(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("description") && dentroItem) {
                        actual.setDescription(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("pubDate") && dentroItem) {
                        actual.setPubDate(xpp.nextText());
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        dentroItem = false;
                        noticias.add(actual);
                    }
                    break;
            }
            eventType = xpp.next();
        }
        return noticias;
    }

    public static void escribirGSON(ArrayList<Noticia> noticias, String fichero) throws IOException {
        OutputStreamWriter out;
        File miFichero;
        Titular titulares;
        String texto;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("dd-MM-yyyy");
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        titulares = new Titular();
        titulares.setWeb("http://www.alejandrosuarez.es/");
        titulares.setFeed("http://www.alejandrosuarez.es/feed/");
        titulares.setTitulares(noticias);
        texto = gson.toJson(titulares);
        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        out = new FileWriter(miFichero);
        out.write(texto);
        out.close();

        Log.i("info", gson.toString());
    }
}
